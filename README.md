## Voici les requis :

Côté client :

Vous devez créer un compte (avec le personnage pour ne pas avoir de problème, au cas où) et retenir les informations suivantes :

- nom de compte (par exemple : web )
- mot de passe (par exemple : web). Récupérer son sha256 : '4B5E57F6EB2F42B9039B3D1E13929295F231749C510CBE341CD68036D9AF97E2' pour le mot de passe : 'web'
    - le site pour récupérer son sha256 : [https://passwordsgenerator.net/sha256-hash-generator/](https://passwordsgenerator.net/sha256-hash-generator/)

/!\ Je vous conseil de changer 'web' par autre chose et de pas avoir le même mot de passe et nom de compte !!

Côté serveur :

- Dans le fichier de configuration : `server/resources/config/api.config.json` : modifie la valeur de api > enable = `true`

Puis, dans la console du serveur, suivre les étapes en remplaçant [ACCOUNT] par le compte que vous avez créé via le client

- api [ACCOUNT] true
- apigrant [ACCOUNT] users.query
- apigrant [ACCOUNT] users.manage


## Comment l'implémenter
```php
    include_once('function/api.functions.php');
    include_once('function/apihelpers.functions.php');

    $server = new APIHelpers(ACCOUNT, PASSWORD_SHA256, PORT = 5400, HOST = 'http://localhost');
```

ATTENTION, la variable [PASSWORD] doit déjà être encodé en SHA256 !! (d'où la récupération en préambule de son SHA256)

Par défaut, le port est 5400 et le host est http://localhost

## L'utilisation

- Connaitre si le serveur est ouvert ou fermé

```php
    $online = $server->ping();
```

- Avoir l'ensemble des stats
```php
    $stat = $server->getStat();
```

- Effectuer une requête (Par exemple validation du password pour un compte)
```php
    $result = $server->action('/api/v1/users/'.ACCOUNT.'/password/validate', 'POST', ["password" => PASSWORD ])->gets();
```

## Tips

Il y a un mini cache pour les éxécutions des requêtes pour éviter le problème : 'Too many request'
