<?php


class APIHelpers
{
    private $username;
    private $password;
    private $port;
    private $host;

    /** @var API */
    private $api;

    public function __construct($username, $passwordSHA256, $port = 5400, $host = 'http://localhost')
    {
        $this->username = $username;
        $this->password = $passwordSHA256;
        $this->port = $port;
        $this->host = $host;
        $this->api = new API($username, $passwordSHA256, $host, $port, md5($port.''.$host));
    }

    public function ping(){
        return $this->api->action('/api/v1/info/stats')->isOnline();
    }

    public function getStats(){
        return $this->api->action('/api/v1/info/stats');
    }

    public function action(String $path, String $method = 'GET', array $param = null){
        return $this->api->action($path, $method, $param);
    }
}