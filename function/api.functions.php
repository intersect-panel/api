<?php
class API{

    private $config = [];

    private $path_config = __DIR__.'/config.json';
    private $config_example = [
        'token' => '',
        'refresh_token' => '',
        'expire' => '',
        'host' => 'http://localhost'
    ];

    private $result;
    
    private $online = null;

    private $username;
    private $password;
    private $host;
    private $port;

    private $cache = [];

    /**
     * API constructor.
     * @param String $username
     * @param String $password
     * @param String $host
     * @param int $port
     * @param String $name_api
     */
    public function __construct(String $username, String $password, String $host = 'http://localhost', int $port = 5400, String $name_api = 'default')
    {
        $this->path_config = __DIR__.'/../config.'.$name_api.'.json';
        $this->username = $username;
        $this->password = $password;
        $this->host = $host;
        $this->port = $port;

        if(file_exists($this->path_config))
        {
            $this->config = json_decode(file_get_contents($this->path_config), true);
        }
        else
        {
            $this->config = $this->config_example;
            $this->saveConfAPI();
        }
        try {
            $this->initAPI();
        } catch (Exception $e) {
            die('Exception to initiliaze API');
        }
    }

    private function saveConfAPI()
    {
        file_put_contents($this->path_config, json_encode($this->config, JSON_PRETTY_PRINT));
    }

    /**
     * @return $this
     * @throws Exception
     */
    private function initAPI()
    {

        $now = new DateTime;
        if($this->config['expire'] != '')
        {
            $dateP = new DateTime;
            $dateP->setTimestamp($this->config['expire']);
            if($dateP < $now)
            {
                $this->getToken();
            }
        }
        else
        {
            $this->getToken();
        }

        return $this;
    }

    /**
     * @throws Exception
     */
    private function getToken(){
        $this->action('/api/oauth/token', 'POST', [
            'grant_type' => 'password',
            'username' => $this->username,
            'password' => $this->password
        ]);
        if($this->get('expires_in') != null){
            $now = new DateTime();
            $this->config['expire'] = $now->add(new DateInterval('PT'.(intval($this->get('expires_in'))-5).'S'))->getTimestamp();
            $this->config['token'] = $this->get('access_token');
            $this->config['refresh_token'] = $this->get('refresh_token');
            $this->saveConfAPI();
        }
    }

    /**
     * @param String $path
     * @param String $method
     * @param null $parameter
     * @return API
     */
    public function action(String $path, String $method = 'GET', $parameter = null)
    {
        $header = "Content-type: application/json";

        if($this->config['token'] != ''){
            $header .= "\r\nAuthorization: Bearer ".$this->config['token'];
        }
        $this->result = null;
        $content = '';
        if(is_array($parameter))
        {
            $content = json_encode($parameter, JSON_PRETTY_PRINT);
        }
        
        try
        {
            $context = stream_context_create([
                'http' => [
                    'header' => $header,
                    'method' => $method,
                    'content' => $content
                ]
            ]);
            if($this->online != null && !$this->isOnline())
                throw new Exception();

            $url = $this->config['host'].':'.$this->port.$path;
            $result = null;

            if(($result = $this->getCache($url)) == null)
            {
                $result = @file_get_contents($url, false, $context);
            }
        }
        catch(Exception $e)
        {
            $this->online = false;
        }
        if($result === false)
        {
            $this->result = null;
        }
        else
        {
            $this->online = true;
            $this->setCache($url, $result);
            $this->result = json_decode($result, true);
        }
        
        return $this;
    }

    /**
     * @param String $key
     * @param $default
     * @return String|$default
     */
    public function get(String $key, $default = null)
    {
        if($this->result == null)
        {
            return $default;
        }
    
        return $this->result[$key];
    }

    /**
     * @return array|null
     */
    public function gets()
    {
        return $this->result;
    }

    /**
     * @return bool
     */
    public function isOnline()
    {
        return $this->online;
    }


    private function setCache(String $id, String $result)
    {
        $this->cache[hash('sha256', $id)] = $result;
    }

    private function getCache(String $id)
    {
        $id = hash('sha256', $id);

        if(!key_exists($id, $this->cache))
        {
            return null;
        }

        return $this->cache[$id];
    }
}
